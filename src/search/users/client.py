import requests

class Client:
    def __init__(self, host: str, port: int):
        self._host = host
        self._port = port

    def get_user_data(self, user_id: int):
        r = requests.get(f"http://{self._host}:{self._port}/user_data", params={
            'user_id': user_id
        })

        return r.json()
