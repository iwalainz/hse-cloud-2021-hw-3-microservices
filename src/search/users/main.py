from users.server import Server
from users.service import Service
from common.data_source import CSV
from settings import USERS_DATA

def main():
    service = Service(CSV(USERS_DATA))
    server = Server('users', service)
    server.run_server(debug=True)

if __name__ == '__main__':
    main()
