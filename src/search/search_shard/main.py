from search_shard.server import Server
from search_shard.service import Service
from common.data_source import CSV
from settings import SHARDS_DATA
import os

def main():
    shard = int(os.getenv("SHARD"))
    service = Service(CSV(SHARDS_DATA[shard]))
    server = Server(f'search_shard{shard}', service)
    server.run_server(debug=True)

if __name__ == '__main__':
    main()
