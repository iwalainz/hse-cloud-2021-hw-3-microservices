from search.server import Server
from search.service import Service
from common.data_source import CSV
from settings import SHARDS_HOSTS

def main():
    service = Service(SHARDS_HOSTS)
    server = Server(f'search', service)
    server.run_server(debug=True)

if __name__ == '__main__':
    main()
