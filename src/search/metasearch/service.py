from typing import List, Dict
from users.client import Client as UsersClient
from search.client import Client as SearchClient


class Service:
    def __init__(self, search: str, users: str) -> None:
        self._search_client = SearchClient(search, 8000)
        self._users_client = UsersClient(users, 8000)

    def search(self, search_text, user_id, limit=10) -> List[Dict]:
        user_data = self._users_client.get_user_data(user_id)
        return self._search_client.get_search_data(search_text, user_data, limit)
