from metasearch.server import Server
from metasearch.service import Service
from settings import SEARCH_HOST, USERS_HOST

def main():
    service = Service(SEARCH_HOST, USERS_HOST)
    server = Server('metasearch', service)
    server.run_server(debug=True)

if __name__ == '__main__':
    main()
